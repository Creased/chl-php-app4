# Mise en place d'un serveur d'application PHP "moderne"

## Préparation du système

Installation d'outils de base&nbsp;:

```bash
apt-get -fy install curl ca-certificates apt-transport-https software-properties-common
```

## Apache HTTP Server

Installation de Apache HTTP Server&nbsp;:

```bash
apt-get -fy install apache2 apache2-utils libapache2-mod-fcgid
```

Préparation des clés et certificat&nbsp;:

```bash
chown ssl-cert:ssl-cert /etc/ssl/mycerts/php-app4.crt
chmod 444 /etc/ssl/mycerts/php-app4.crt
openssl rsa </etc/ssl/private/php-app4.miletrie.chl-key.pem /etc/ssl/private/php-app4.key
chgrp ssl-cert /etc/ssl/private/php-app4.*
chmod 440 /etc/ssl/private/php-app4.miletrie.chl-key.pem
```

Activation des modules, désactivation des sites par défauts et activation des configurations pour PHP-FPM&nbsp;:

```bash
a2enmod proxy proxy_fcgi setenvif ssl headers rewrite actions
a2query -m
a2dissite 000-default.conf
a2enconf php7.0-fpm
systemctl restart apache2.service
```

Création d'un site par défaut&nbsp;:

```bash
cat <<-'EOF' >/etc/apache2/sites-available/default.conf
<VirtualHost *:80>
    RewriteEngine           On
    RewriteCond             %{HTTPS} off
    RewriteRule             (.*) https://php-app4.miletrie.chl%{REQUEST_URI}
</VirtualHost>

<VirtualHost *:443>
    DocumentRoot            /var/www/html
    ServerName              php-app4.miletrie.chl

    <DirectoryMatch "^/var/www/html">
        Options             -Indexes -FollowSymLinks -MultiViews
        AllowOverride       None
        Require             all granted
    </DirectoryMatch>

    SSLEngine               On

    SSLCipherSuite          !EDH:!EXP:!SHA:!DSS:EECDH+ECDSA+AESGCM:EECDH+ECDSA+SHA384:EECDH+ECDSA+SHA256:EECDH+AESGCM:EECDH+SHA384:EECDH+SHA256
    SSLHonorCipherOrder     On

    SSLCompression          Off

    SSLProtocol             TLSv1.2
    SSLCertificateFile      /etc/ssl/mycerts/php-app4.crt
    SSLCertificateKeyFile   /etc/ssl/private/php-app4.key

    SSLSessionTickets       Off

    LogLevel                info ssl:warn
    ErrorLog                ${APACHE_LOG_DIR}/error.log
    # CustomLog               ${APACHE_LOG_DIR}/access.log combined

    Header                  always set Strict-Transport-Security "max-age=31536000; preload"
    Header                  always set X-Frame-Options "SAMEORIGIN"
    Header                  always set X-Content-Type-Options "nosniff"
    Header                  always set X-Xss-Protection "1; mode=block"
    Header                  always set Referrer-Policy "no-referrer-when-downgrade"
    Header                  always set Content-Security-Policy "default-src 'none'; script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; img-src 'self' data: https:; font-src 'self' https:; connect-src 'self'; media-src 'self'; object-src 'none'; child-src 'none'; frame-src 'none'; worker-src 'self'; frame-ancestors 'self'; form-action 'self'; upgrade-insecure-requests; block-all-mixed-content; base-uri 'self' about:; manifest-src 'self' 'self'; referrer no-referrer-when-downgrade; require-sri-for script"
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF
cat <<-'EOF' >/etc/apache2/conf-available/log.conf
# Log all requests to access.log
CustomLog ${APACHE_LOG_DIR}/access.log vhost_combined

EOF
a2ensite default
a2disconf other-vhosts-access-log
a2enconf log
```

## PHP-FPM

PHP-FPM (**PHP FastCGI Process Manager**) est une alternative à PHP FastCGI embarquant de nouvelles fonctionnalités&nbsp;:

 - Gestion avancée des processus avec stop/start doux *(&laquo;&nbsp;graceful&nbsp;&raquo;)*&nbsp;;
 - Possibilité de démarrer des processus avec différents uid/gid/chroot/environment, écoutant sur différents ports et utilisant différents php.ini&nbsp;;
 - Journalisation stdout et stderr&nbsp;;
 - Support de l'upload acccéléré&nbsp;;
 - Fichier de configuration basé sur php.ini.

Installation de PHP-FPM&nbsp;:

```bash
apt-get -fy install php7.0-cli php-pear
systemctl restart php7.0-fpm.service
php -v
```

## Application PHP

Création du skel pour les nouveaux utilisateurs&nbsp;:

```bash
install -m 750 -o root -g root -d /etc/skel-php{,/{etc{,/{php,apache2}},bin,classes,conf,log,tmp,www}}/
install -m 770 -o root -g root -d /etc/skel-php/{log,tmp}/
cat <<-'EOF' >/etc/skel-php/www/index.php
<?php
    phpinfo();
?>

EOF
```

Création d'une application PHP&nbsp;:

```bash
./create_app.sh
```
