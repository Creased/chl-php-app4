#!/bin/bash

#==========================================================#
# [+] Title:   PHP-FPM Application creator                 #
# [+] Author:  Baptiste M. (Creased)                       #
# [+] Website: bmoine.fr                                   #
# [+] Email:   contact@bmoine.fr                           #
# [+] Twitter: @Creased_                                   #
#==========================================================#

##
# Functions
#
function print() {
    if (( $# == 1 )); then
        ###
        # Variables
        #
        STR=${1}

        ###
        # Main process
        #
        printf "%b${STR}\n" >&3
    fi
}

function spin() {
    if (( $# == 2 )); then
        ###
        # Variables
        #
        PID=${1:-''}
        MESSAGE=${2:-''}
        INTERVAL=.08s
        SPINNER="⠋⠙⠚⠞⠖⠦⠴⠲⠳⠓"

        ###
        # Main process
        #
        if [ ! -z "${PID}" ]; then
            printf "\033[?25l" >&3
            while [ -d /proc/${PID} ]; do
                printf "\033[2K\033[G[+] ${MESSAGE}\033[0m [ ]\033[1D" >&3
                printf "\033[D${SPINNER:i++%${#SPINNER}:1}" >&3
                sleep ${INTERVAL}
            done
            printf "\033[?25h\033[C\033[2K\033[G" >&3
        fi
    fi
}

function create-user() {
    if (( $# == 1 )); then
        ###
        # Variables
        #
        APP=${1}
        ANSW="y"

        ###
        # Main process
        #
        if [[ $(getent passwd | grep "^php-${APP}" | awk -F':' '{print $1}') == "php-${APP}" ]]; then
            printf "\033[1;36mL'utilisateur ${APP} existe déjà, voulez-vous le supprimer \033[0m[\033[1;31my\033[0m/\033[1;32mn\033[0m]\033[1;36m ?\033[0m\033  "
            read ANSW
        fi

        if [[ "${ANSW}" == "y" ]]; then
            remove-user "${APP}"
        fi

        ANSW="y"

        if [[ -d /home/php-${APP} ]]; then
            printf "\033[1;36mL'application ${APP} existe déjà, voulez-vous la supprimer \033[0m[\033[1;31my\033[0m/\033[1;32mn\033[0m]\033[1;36m ?\033[0m\033  "
            read ANSW
        fi

        if [[ "${ANSW}" == "y" ]]; then
            remove-app "${APP}"
        fi

        useradd -c "Utilisateur PHP ${APP}" -d /home/php-${APP} -g www-data -k /etc/skel-php -K UMASK=0027 -m -N -s /bin/false php-${APP}
    fi
}

function create-php-fpm() {
    if (( $# == 1 )); then
        ###
        # Variables
        #
        APP=${1}
        ANSW="y"

        ###
        # Main process
        #
        if [[ -f /home/php-${APP}/etc/php/php-fpm.conf ]]; then
            printf "\033[1;36mLa configuration PHP pour l'application ${APP} existe déjà, voulez-vous la supprimer \033[0m[\033[1;31my\033[0m/\033[1;32mn\033[0m]\033[1;36m ?\033[0m\033  "
            read ANSW
        fi

        if [[ "${ANSW}" == "y" ]]; then
cat <<-EOF >/home/php-${APP}/etc/php/php-fpm.conf
; PHP-FPM Configuration for ${APP} application
[global]
pid = /var/run/php/php7.0-fpm-${APP}.pid
error_log = /home/php-${APP}/log/error.log

[www-${APP}]
user = php-${APP}
group = www-data

listen = /run/php/php7.0-fpm-${APP}.sock
listen.owner = www-data
listen.group = www-data
listen.mode = 0600

pm = ondemand
pm.max_children = 15
pm.process_idle_timeout = 10s
pm.max_requests = 450

php_admin_value["log_errors"] = On
php_admin_value["upload_tmp_dir"] = /home/php-${APP}/tmp/
php_admin_value["include_path"] = /home/php-${APP}/classes/

EOF
        fi

        ANSW="y"

        if [[ -f /lib/systemd/system/php7.0-fpm.${APP}.service ]]; then
            printf "\033[1;36mLe service php7.0-fpm.${APP}.service existe déjà, voulez-vous le supprimer \033[0m[\033[1;31my\033[0m/\033[1;32mn\033[0m]\033[1;36m ?\033[0m\033  "
            read ANSW
        fi

        if [[ "${ANSW}" == "y" ]]; then
            SERVICE_PATH=/lib/systemd/system/
            cp ${SERVICE_PATH}/php7.0-fpm{,.${APP}}.service
            ln -s ${SERVICE_PATH}/php7.0-fpm.${APP}.service /etc/systemd/system/multi-user.target.wants/php7.0-fpm.${APP}.service
            perl -p -i -e '
                s@(^PIDFile[\s\t]*=[\s\t]*).+@\1/var/run/php/php7.0-fpm-'${APP}'.pid@;
                s@(--fpm-config )[^\s]+@\1/home/php-'${APP}'/etc/php/php-fpm.conf@
            ' ${SERVICE_PATH}/php7.0-fpm.${APP}.service
            systemctl daemon-reload
            systemctl start php7.0-fpm.${APP}.service
        fi
    fi
}

function create-apache-conf() {
    if (( $# == 1 )); then
        ###
        # Variables
        #
        APP=${1}
        ANSW="y"

        ###
        # Main process
        #
        if [[ -f /home/php-${APP}/etc/apache2/app.conf ]]; then
            printf "\033[1;36mLa configuration Apache HTTP pour l'application ${APP} existe déjà, voulez-vous la supprimer \033[0m[\033[1;31my\033[0m/\033[1;32mn\033[0m]\033[1;36m ?\033[0m\033  "
            read ANSW
        fi

        if [[ "${ANSW}" == "y" ]]; then
cat <<-EOF >/home/php-${APP}/etc/apache2/app.conf
AliasMatch              "(?i)^/${APP}(.*)" "/home/php-${APP}/www\$1"

SetEnvIf                Request_URI "^/${APP}/" log-${APP}
CustomLog               /home/php-${APP}/log/access.log combined env=log-${APP}

<DirectoryMatch "^/home/php-${APP}/www">
    Options             -Indexes -FollowSymLinks -MultiViews
    AllowOverride       None
    Require             all granted
    <FilesMatch "^.+\.ph(p[3457]?|t|tml)$">
        SetHandler      "proxy:unix:/run/php/php7.0-fpm-${APP}.sock|fcgi://localhost"
    </FilesMatch>
</DirectoryMatch>

EOF
            ln -s /home/php-${APP}/etc/apache2/app.conf /etc/apache2/conf-enabled/php-${APP}.conf
            systemctl reload apache2.service
        fi
    fi
}

function remove-all-apps() {
    ###
    # Main process
    #
    remove-app "*"
}

function remove-app() {
    if (( $# == 1 )); then
        ###
        # Variables
        #
        APP=${1}

        ###
        # Main process
        #
        systemctl stop php7.0-fpm.${APP}.service 2>/dev/null
        rm -rf /lib/systemd/system/php7.0-fpm.${APP}.service /etc/systemd/system/multi-user.target.wants/php7.0-fpm.${APP}.service /etc/apache2/conf-enabled/php-${APP}.conf /home/php-${APP}/
        for USER in $(getent passwd | grep "^php-${APP}" | awk -F':' '{print $1}'); do
            userdel -f ${USER}
        done
        systemctl daemon-reload
    fi
}

function remove-all-users() {
    ###
    # Main process
    #
    remove-user "*"
}

function remove-user() {
    if (( $# == 1 )); then
        ###
        # Variables
        #
        APP=${1}

        ###
        # Main process
        #
        for USER in $(getent passwd | grep "^php-${APP}" | awk -F':' '{print $1}'); do
            userdel -f ${USER}
        done
    fi
}

##
# Variables
#
# exec 3>/dev/stdout
# exec 2>&1
# exec 1>/dev/null

###
# User input (Application name)
#
printf "\033[1;35mSaisissez le nom de l'application à créer :\033[0m\n\033[1;36m>\033[0m " >&3
read APP
APP=${APP:-test}

###
# Main process
#
create-user "${APP}"
# create-user "${APP}" &
# PID="${!}"
# spin "${PID}" "Création de l'utilisateur ${APP}"

create-php-fpm "${APP}"
# create-php-fpm "${APP}" &
# PID="${!}"
# spin "${PID}" "Configuration de PHP-FPM pour l'application ${APP}"

create-apache-conf "${APP}"
# create-apache-conf "${APP}" &
# PID="${!}"
# spin "${PID}" "Création d'une configuration Apache HTTP pour l'application ${APP}"

exit 0
